#!/bin/bash


# $ VBoxManage showmediuminfo Lifi-20221121.vdi 
# UUID:           4e5152d7-64be-4014-a994-5cac35879087
# Parent UUID:    base
# State:          created
# Type:           normal (base)
# Location:       /home/jake/stuff/lifi/lifi-builder/test/orig-lifi/Lifi-20221121/Lifi-20221121.vdi
# Storage format: VDI
# Format variant: dynamic default
# Capacity:       40000 MBytes
# Size on disk:   7654 MBytes
# Encryption:     disabled
# Property:       AllocationBlockSize=1048576

# $ VBoxManage internalcommands dumphdinfo Lifi-20221121.vdi 
# --- Dumping VD Disk, Images=1
# Dumping VD image "Lifi-20221121.vdi" (Backend=VDI)
# Dumping VDI image "Lifi-20221121.vdi" mode=r/o uOpenFlags=9 File=0x005643081a6500
# Header: Version=00010001 Type=1 Flags=0 Size=41943040000
# Header: cbBlock=1048576 cbBlockExtra=0 cBlocks=40000 cBlocksAllocated=7652
# Header: offBlocks=1048576 offData=2097152
# Header: Geometry: C/H/S=1024/255/63 cbSector=512
# Header: uuidCreation={4e5152d7-64be-4014-a994-5cac35879087}
# Header: uuidModification={d2ed2aea-3a09-4cef-9eaf-b755e9f1a98e}
# Header: uuidParent={00000000-0000-0000-0000-000000000000}
# Header: uuidParentModification={00000000-0000-0000-0000-000000000000}
# Image:  fFlags=00000000 offStartBlocks=1048576 offStartData=2097152
# Image:  uBlockMask=000FFFFF cbTotalBlockData=1048576 uShiftOffset2Index=20 offStartBlockData=0


set -e

pushd "$(dirname "$0")" >/dev/null
source ./common.sh

### Hardcode some settings about the Deployment Image so the E-Prüfungsraum can use it, because ..... really weird setups.
RELEASENAME="Lifi-20221121"
RELEASEDATE="20221121"
VDIUUID="4e5152d7-64be-4014-a994-5cac35879087"
MACHINEUUID="20221121-71f1-71f1-71f1-71f100001234"

EXAMVDI="${RELEASENAME}.vdi"

function main() {

	info "Exam Releases:"
	RELEASES="$(find releases -type f | grep '\.zip$' | grep -i 'Exam' | sed 's,\(.*\)-\([0-9]*\).zip$,\2@\1-\2.zip,g' | sort | cut -d '@' -f 2 | tac | nl | tac | sed 's,^ *,,' | tr '\t' ' ')"
	
	echo "$RELEASES" | tr ' ' '\t' | sed 's,^,\t,'
	
	maxreleasenum="$(echo "${RELEASES}" | wc -l)"
	
	SEL=0
	#SEL=1
	until [[ "${SEL}" =~ ^[0-9][0-9]*$ ]] && [ "$SEL" -ge 1 ] && [ "$SEL" -le "${maxreleasenum}" ] ; do
		question "Please select a release number to deploy[1-${maxreleasenum}](Newest: 1): "
		read -r SEL
	done
	
	SELFILE="$(echo "${RELEASES}" | grep "^${SEL} " | cut -d ' ' -f 2)"
	
	echo -e "\n"
	info "===>>> Selected image for deployment: $SELFILE ($SEL) <<<==="
	echo -e "\n"
	warn "Please verify that the correct image was selected!"
	question "Should this image be deployed to the E-Prüfungsraum?\n"
	
	if ask_yon ; then
		true
	else
		die "User doesn't want to deploy this image."
	fi
	
	info "You may need to enter a password later."
	
	task "Unzipping release"
	rm -rf build/deploy
	mkdir -p build/deploy
	unzip "$SELFILE" -d build/deploy
	
	pushd build/deploy
	
	task "Building deploy directory"
	find . -type f | tee origreleasefiles.txt
	mv -v "$(find . -type f | grep '\.vdi$' | head -n 1)" "${EXAMVDI}" # move unzipped vdi file
	rm -rvf $(find . -type d | grep 'Lifi' | grep 'Exam') # remove unzipped directory
	cat ../../template-deploy.vbox | sed "s,YYYYMMDD,${RELEASEDATE},g;s,MACHINEUUID,${MACHINEUUID},g;s,VDIUUID,${VDIUUID},g;s%RELEASENAME%${RELEASENAME}%g" > "${RELEASENAME}.vbox" # create vbox file
	ls -lah .
	
	task "Changing UUID of disk"
	VBoxManage internalcommands sethduuid "${EXAMVDI}" "${VDIUUID}" # set UUID of disk
	dumpvdidebuginfo "${EXAMVDI}"
	
	task "Set file permissions"
	chmod u+rw,g+rw,o+r "${EXAMVDI}"
	
	task "Hash files"
	sha256sum * | tee sha256sums.txt
	
	echo -ne "\n"
	task "Uploading files to deployment server"
	info "You may need to enter a password now."
	#rsync -v -r --progress --perms  . ckurs-exam@elearn.informatik.uni-goettingen.de:/home/ckurs-exam/test/
	rsync -v -r --progress --perms  . ckurs-exam@elearn.informatik.uni-goettingen.de:/home/ckurs-exam/lifi/
	
	popd
	
	echo ""
	info "==>> Finished deploying '${SELFILE}' to the E-Prüfungsraum! <<=="
}

mkdir -p logs
DT="$(date +%Y%m%d-%H%M%S)"
LOGFILE="logs/deploy-exam-${DT}.log"
debug "Logfile: $LOGFILE"
main 2>&1 | tee -a "${LOGFILE}"

popd >/dev/null
