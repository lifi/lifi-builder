#!/bin/bash

# VBoxManage Documentation: https://docs.oracle.com/en/virtualization/virtualbox/6.0/user/vboxmanage.html

set -e

pushd "$(dirname "$0")"

source ./common.sh

#echo "Running VMS:"
#VBoxManage list runningvms
#echo "All VMS:"
#VBoxManage list vms


# Poweroff & Unregister
if VBoxManage list runningvms | grep -q -F "$UUID" ; then
	warn "VM is still running"
	task "Powering down old VM"
	VBoxManage controlvm $UUID poweroff
	waitforvmshutdown
fi
if VBoxManage list vms | grep -q -F "$UUID" ; then
	task "Removing old VM"
	VBoxManage unregistervm $UUID --delete
fi

task "Removing old .vdi File"
if VBoxManage closemedium disk "${LIFIVDI}" --delete ; then
	info "Deleted old .vdi File"
else
	if [ -f "${LIFIVDI}" ]; then
		rm -fv "${LIFIVDI}"
	fi
fi

task "Creating VM"
#VBoxManage createvm --name lifi-builder --groups /lifi --ostype Ubuntu_64 --basefolder ./base/ --uuid $UUID --default --register
VBoxManage createvm --name lifi-builder --groups /lifi --ostype Ubuntu_64 --basefolder ./base/ --uuid $UUID --register

task "Creating .vdi File"
VBoxManage createmedium disk --filename "${LIFIVDI}" --format VDI --size ${LIFIVDISIZEMB}

dumpvdidebuginfo

task "Setting up virtual Hardware of VM"
VBoxManage modifyvm $UUID --vram 128
VBoxManage modifyvm $UUID --memory 4096
VBoxManage modifyvm $UUID --cpus 10
VBoxManage modifyvm $UUID --pae off
VBoxManage modifyvm $UUID --rtcuseutc on
VBoxManage modifyvm $UUID --audio none
VBoxManage modifyvm $UUID --graphicscontroller vmsvga
VBoxManage modifyvm $UUID --boot1 disk --boot2 dvd --boot3 none --boot4 none
VBoxManage modifyvm $UUID --natpf1 "guestssh,tcp,,${SSHPORT},,22" # Port Forwarding
# Storage Controllers
VBoxManage storagectl $UUID --name IDE --add ide --controller PIIX4 --portcount 2 --bootable on
VBoxManage storagectl $UUID --name SATA --add sata --controller IntelAhci --portcount 1 --bootable on

task "Attach ${LIFIISO} as dvd to VM"
VBoxManage storageattach $UUID --storagectl IDE  --medium "${LIFIISO}" --type dvddrive --port 1 --device 0

task "Attach ${LIFIVDI} as harddrive to VM"
VBoxManage storageattach $UUID --storagectl SATA  --medium "${LIFIVDI}" --type hdd --port 0 --device 0

# Starting VM for inital install
startvm


info "VM started and should be installing the base image right now. This will take a long time. The VM will shutdown once the base image is installed."
waitforvmshutdown
info "VM finished initial base install."

dumpvdidebuginfo

# Starting VM for running the Ansible role"
startvm


if [ "x${LIFIBUILDINDEVMODE}y" = "xtruey" ]; then
	info "LIFIBUILDINDEVMODE is set to true."
	warn "This will create a release that is already in dev mode. Only do this when developing! Never in an actual release!"
	info "If you want to undo this, you have to execute make clean-all and set LIFIBUILDINDEVMODE to false!"
	question "Are you 100% sure you want to create a release that will already be switched into dev mode?"
	if ask_yon ; then
		task "Switiching Lifi to dev mode."
		# In den Entwicklungsbranch wechseln
		lifissh "/bin/bash -c \"cd /opt/lifi && sudo git switch dev\""
		# Entwicklungsmodus aktivieren
		lifissh "sudo touch /opt/lifi/.dev"
		# Automatisches 'git pull' im Entwicklungsmodus reaktivieren
		lifissh "sudo touch /opt/lifi/.dev-update"
	else
		die "User declined wanting to build a release in dev mode."
	fi
else
	info "LIFIBUILDINDEVMODE is not set to true. => Not switching to dev mode."
fi

task "Running Ansible Role..."
lifissh "/bin/bash -c \"/opt/lifi/files/update\""

task "Tell people to update after installing Lifi"
lifissh "/bin/bash -c \"echo 0 | sudo tee /var/opt/lifi_last_successful_update\""

task "Shutting down VM."
lifissh "/bin/bash -c \"sudo shutdown -h now\"" || true

waitforvmshutdown

popd

info "Done setup-vbox.sh."
