#!/bin/bash

set -e

pushd "$(dirname "$0")"

source ./common.sh

export RELEASEDATE="$(date +%Y%m%d)"
if [ "$1" = "exam" ]; then
	export RELEASENAME="Lifi-Exam-${EXAMID}-${RELEASEDATE}"
	export TEMPLATEFILE="../template-exam.vbox"
else
	export RELEASENAME="Lifi-${RELEASEDATE}"
	export TEMPLATEFILE="../template.vbox"
fi

task "Creating Release '${RELEASENAME}' ..."

pushd build


if [ -d "${RELEASENAME}" ]; then
	task "Removing old release from today"
	rm -rvf "${RELEASENAME}"
fi
mkdir -p "${RELEASENAME}"

dumpvdidebuginfo

task "Copying VDI File..."
VBoxManage clonevdi "${LIFIVDI}" "${RELEASENAME}/${RELEASENAME}.vdi"

dumpvdidebuginfo "${RELEASENAME}/${RELEASENAME}.vdi"

export VDIUUID="$(VBoxManage showmediuminfo "${RELEASENAME}/${RELEASENAME}.vdi"  | grep '^UUID:' | sed 's,  *,,g' | cut -d ':' -f 2)"

# Remove new vdi file from Virtualbox to avoid future problems
VBoxManage closemedium disk "${RELEASENAME}/${RELEASENAME}.vdi"

task "Creating Configuration..."

cat "${TEMPLATEFILE}" | sed "s,YYYYMMDD,${RELEASEDATE},g;s,VDIUUID,${VDIUUID},g;s%RELEASENAME%${RELEASENAME}%g" > "${RELEASENAME}/${RELEASENAME}.vbox"

task "Zipping Release..."
zip -Z deflate -6 -v -r "${RELEASENAME}.zip" "${RELEASENAME}"

date

popd

mkdir -p releases

task "Moving into releases/"
mv -v "build/${RELEASENAME}.zip" releases/

pushd releases

task "Adding zipinfo"
zipinfo -l "${RELEASENAME}.zip" | tee "${RELEASENAME}.zipinfo"
task "Adding sha256sum"
sha256sum "${RELEASENAME}.zip" | tee "${RELEASENAME}.sha256sum"

popd

popd

echo ""
echo ""
echo ""
info "==>> Finished creating release '${RELEASENAME}' <<=="
