#!/bin/bash


set -Eeuo pipefail # Exit on error

pushd "$(dirname "$0")"

REMOTEBACKUPDIR="/usr/users/a/doemming1/arbeit/lifi/releases/"
LOCALTOBACKUPDIR="./releases/"

source ./common.sh


function main() {
	task "Creating backups of releases..."

	# Zu langsam, da Tape.
	# rsync -r -a -h -progress -e ssh -v ./releases/ GWDGUnixClusterTransfer:/usr/users/a/doemming1/arbeit/lifi/releases/

	task "Getting remote file list...."
	REMOTECMDS="\
		mkdir -p -v \"${REMOTEBACKUPDIR}\";\
		cd \"${REMOTEBACKUPDIR}\";\
		find . -type f ;\
		"
	#echo "${REMOTECMDS}"
	REMOTEFILELIST="$(ssh GWDGUnixClusterLogin "${REMOTECMDS}" | sort -u)"
	info "Got remote file list."
	#echo "${REMOTEFILELIST}"

	pushd "${LOCALTOBACKUPDIR}" > /dev/null
	LOCALFILELIST="$(find  . -type f | sort -u)"
	info "Got local file list."
	#echo "${LOCALFILELIST}"

	DIFF="$(diff <(echo "${LOCALFILELIST}") <(echo "${REMOTEFILELIST}") || true)"
	info "Diff of file lists:"
	echo "$DIFF"

	TOUPLOAD="$(echo "${DIFF}" | grep '^<' | sed 's,^< ,,')"
	if [ -z "$TOUPLOAD" ]; then
		info "There is nothing to backup. I am done here."
		exit 0
	fi

	info "The following files got marked for uploading:"
	echo "${TOUPLOAD}"

	DIRSTOCREATE="$(echo "${TOUPLOAD}" | while read line ; do echo "$(dirname "${line}")"; done | sort -u)"
	info "The following directories will be created on the remote machine:"
	echo "${DIRSTOCREATE}"

	task "Creating directories on remote machine..."
	REMOTECMDS="\
		mkdir -p -v \"${REMOTEBACKUPDIR}\";\
		cd \"${REMOTEBACKUPDIR}\";\
		$(echo "${DIRSTOCREATE}" | sed 's,^,mkdir -p -v ",;s,$,";,' | tr '\n' ' ')\
		"
	ssh GWDGUnixClusterLogin "${REMOTECMDS}" | sort -u
	info "Directories created."

	task "Uploading files..."
	echo "${TOUPLOAD}" | while read line ; do
		REMOTEPATH="GWDGUnixClusterTransfer:${REMOTEBACKUPDIR}${line}"
		info "Uploading: ${line} -> ${REMOTEPATH}"
		scp -s "$line" "${REMOTEPATH}"
	done
	info "Uploaded files."

	popd > /dev/null

	echo ""
	echo ""
	info "==>> Finished creating backups. <<=="
}

mkdir -p logs
DT="$(date +%Y%m%d-%H%M%S)"
LOGFILE="logs/backup-${DT}.log"
debug "Logfile: $LOGFILE"
main 2>&1 | tee -a "${LOGFILE}"

popd >/dev/null
