#!/bin/bash

set -Eeuo pipefail

pushd "$(dirname "$0")"

source ./common.sh

mkdir -p downloads
pushd downloads
if [ ! -f ubuntu.iso ] || ! sha256sum ubuntu.iso | grep -q "c396e956a9f52c418397867d1ea5c0cf1a99a49dcf648b086d2fb762330cc88d"; then
	task "Downloading Ubuntu iso"
	wget https://releases.ubuntu.com/22.04.1/ubuntu-22.04.1-desktop-amd64.iso -O ubuntu.iso
fi
popd

pushd build

if [ ! -f ubuntu.iso ]; then
	ln -s ../downloads/ubuntu.iso ubuntu.iso
fi

if [ ! -d "./extract-cd" ]; then
	#rm -rvf ./extract-cd
	mkdir -p extract-cd
	task "Extracting ubuntu.iso into ./extract-cd"
	xorriso -osirrox on -indev ubuntu.iso -extract / "./extract-cd" &>/dev/null
	rm -rvf 'extract-cd/[BOOT]'
fi

chmod -R u+w "extract-cd"

task "Configure automatic installation with lifi.preseed "
cp -vf ../lifi.seed extract-cd/preseed/

task "Copy isolinux"
mkdir -p extract-cd/isolinux
cp -vf ../syslinux/bios/core/isolinux.bin extract-cd/isolinux/
cp -vf ../syslinux/bios/com32/elflink/ldlinux/ldlinux.c32 extract-cd/isolinux/
cp -vf ../isolinux.cfg extract-cd/isolinux/

task "Copy filesystem.manifest*"
cat ../filesystem.manifest                | grep -v '^#' > extract-cd/casper/filesystem.manifest
cat ../filesystem.manifest-minimal-remove | grep -v '^#' > extract-cd/casper/filesystem.manifest-minimal-remove
cat ../filesystem.manifest-remove         | grep -v '^#' > extract-cd/casper/filesystem.manifest-remove

task "Removing md5sum.txt"
rm -vf extract-cd/md5sum.txt
#task "Update md5sum.txt"
#pushd extract-cd
#rm md5sum.txt
#find -type f -print0 | xargs -0 md5sum | grep -v isolinux/boot.cat | tee md5sum.txt
#popd

task "Creating the ISO Image"
if [ -f lifi.iso ]; then rm lifi.iso ; fi
mkisofs -D -r -V "Lifi Installer" -cache-inodes -J -l -b isolinux/isolinux.bin -c boot.catalog -input-charset utf-8 -no-emul-boot -boot-load-size 4 -boot-info-table -o lifi.iso extract-cd/

mv -vf lifi.iso ..
info "Created lifi.iso!!!!"

popd
popd

info "Done generate-iso.sh."
