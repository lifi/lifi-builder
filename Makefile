
.PHONY: default
#default: lifi.iso
default: lifi.vdi
#default: test

.PHONY: test
test: exam
	#./compact-image.sh
	#./setup-vbox.sh
#	./test.sh

.PHONY: release
release: lifi.vdi
	date
	./release.sh
	date


.PHONY: exam
exam: lifi.vdi
	date
	./exam.sh
	date

.PHONY: release-exam
release-exam: exam
	date
	./release.sh exam
	date

.PHONY: deploy-exam
deploy-exam:
	date
	./deploy-exam.sh
	date

lifi.vdi: setup-vbox.sh lifi.iso
	date
	./setup-vbox.sh
	./compact-image.sh
	date


lifi.iso: generate-iso.sh syslinux/bios/core/isolinux.bin lifi.seed isolinux.cfg filesystem.manifest filesystem.manifest-minimal-remove filesystem.manifest-remove
	date
	./generate-iso.sh

syslinux/bios/core/isolinux.bin:
	date
	mkdir -p syslinux
	rm -rf syslinux
	wget https://mirrors.edge.kernel.org/pub/linux/utils/boot/syslinux/syslinux-6.03.tar.gz -O syslinux.tar.gz
	tar xzf syslinux.tar.gz
	mv -v "syslinux-6.03" syslinux
	rm -vf syslinux.tar.gz

.PHONY: clean-all
clean-all: clean
	rm -rf syslinux
	rm -rvf lifi.vdi lifi.iso

.PHONY: clean
clean:
	rm -rf build

.PHONY: backup
backup:
	date
	./backup.sh
	date
