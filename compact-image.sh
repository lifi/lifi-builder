#!/bin/bash

# https://superuser.com/questions/529149/how-to-compact-virtualboxs-vdi-file-size

set -e

pushd "$(dirname "$0")"

source ./common.sh


info "Old VM size:"
ls -lah "${LIFIVDI}"


startvm

dumpvdidebuginfo

task "Removing old bloated swapfile"
lifissh "/bin/bash -c \"sudo swapoff /swapfile ; sudo rm -f /swapfile\""

task "Filling virtual disk with zeros until no space left. Nullifing free space."
lifissh "/bin/bash -c \"dd if=/dev/zero of=/var/tmp/bigemptyfile bs=4096k ; rm -f /var/tmp/bigemptyfile\""

task "Creating new swapfile"
lifissh "/bin/bash -c \"sudo install -o root -g root -m 0600 /dev/null /swapfile ; sudo fallocate -l 4G /swapfile ; sudo mkswap /swapfile \""
#lifissh "/bin/bash -c \"sudo install -o root -g root -m 0600 /dev/null /swapfile ; sudo fallocate -l 4G /swapfile ; sudo mkswap /swapfile ; sudo swapon /swapfile \""

task "Shutting down VM."
lifissh "/bin/bash -c \"sudo shutdown -h now\"" || true

waitforvmshutdown

task "Compacting VM..."
VBoxManage modifymedium --compact "${LIFIVDI}"

dumpvdidebuginfo

info "New VM size:"
ls -lah "${LIFIVDI}"

popd
info "Done compact-image.sh."
