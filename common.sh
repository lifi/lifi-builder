#!/bin/bash

set -e

pushd "$(dirname "$0")" >/dev/null

export SSHPORT=7161
export BOOTTIMEOUTSEC=6000
export UUID=01234567-71f1-71f1-71f1-899876543210
export LIFIVDISIZEMB=40000
export LIFIISO="$(readlink -f ./lifi.iso)"
export LIFIVDI="$(echo "${LIFIISO}" | sed 's,\.iso$,.vdi,')"

mkdir -p build
export BUILDDIR="$(readlink -f ./build)"

if [ -L "./current_exam_config" ]; then
	python3 -m venv venv
	source venv/bin/activate
	pip3 install shyaml
	export EXAMDIR="$(readlink -f ./current_exam_config)"
	export EXAMID="$(cat "${EXAMDIR}/config.yaml"  | shyaml get-value exam.ID)"
fi

function msgnn() { # no newline
	echo -n -e "\e[1;33m[$(date +%H:%M:%S)] \e[0m${@}"
}
export -f msgnn

function msg() {
	msgnn "${@}\n"
}
export -f msg

function info() {
	msg "\e[36mINFO: \e[0m${@}"
}
export -f info

function task() {
	msg "\e[33mTASK: \e[0m${@}" >&2
}
export -f task

function question() {
	msgnn "\e[35mQUESTION: \e[0m${@}" >&2
}
export -f question

function debug() {
	msg "\e[34mDEBUG: \e[0m${@}" >&2
}
export -f debug

function warn() {
	msg "\e[33mWARNING: \e[0m${@}" >&2
}
export -f warn

function err() {
	echo -ne "\n"
	msg "\e[31mERROR: \e[0m${@}" >&2
	echo -ne "\n"
}
export -f err

function die() {
	echo -ne "\n"
	msg "\e[31mFATAL ERROR: \e[0m${@}" >&2
	echo -ne "\n"
	exit 1
}
export -f die


function ask_yon() {
	local yon=""
	while true ; do
		printf '\r'
		read -p "[Y/N]? " -N 1 -r yon
		if [ "x${yon}" = "xy" ] || [ "x${yon}" = "xY" ]; then
			printf '\n'
			true; return
		elif [ "x${yon}" = "xn" ] || [ "x${yon}" = "xN" ]; then
			printf '\n'
			false; return
		fi
	done
}
export -f ask_yon


function lifissh() {
	# Remove old ./build/known_hosts
	rm -f "${BUILDDIR}/known_hosts"
	# SSH Connection
	sshpass -p lifi ssh -o "ConnectTimeout=${BOOTTIMEOUTSEC}" -o "StrictHostKeyChecking=no" -o UserKnownHostsFile="${BUILDDIR}/known_hosts" -p $SSHPORT lifi@127.0.0.1 "${@}"
}
export -f lifissh

function lifiscp() {
	# Usage: lifiscp <source on host> <target in lifi>
	# Example:
	# lifiscp ./current_exam /tmp/exam_configuration

	# Remove old ./build/known_hosts
	rm -f "${BUILDDIR}/known_hosts"
	# Upload file or directory
	sshpass -p lifi scp -o "ConnectTimeout=${BOOTTIMEOUTSEC}" -o "StrictHostKeyChecking=no" -o UserKnownHostsFile="${BUILDDIR}/known_hosts" -P $SSHPORT -r -v "$1" "lifi@127.0.0.1:$2"
}
export -f lifiscp


function waitforvmshutdown() {
	while VBoxManage showvminfo $UUID | grep "^State" | grep -q running ; do
		info "Waiting for VM to shutdown..."
		sleep 5
	done
	sleep 10
}
export -f waitforvmshutdown

function startvm() {
	info "Starting VM..."
	VBoxManage startvm $UUID --type gui
}
export -f startvm

function dumpvdidebuginfo() {
	if [ "$#" = "0" ]; then
		dumpvdidebuginfo "${LIFIVDI}"
		return
	fi
	task "Dump debugging internal information about ${1}"
	VBoxManage showmediuminfo "${1}"
	VBoxManage internalcommands dumphdinfo "${1}"
}
export -f dumpvdidebuginfo



popd >/dev/null
