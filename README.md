# Lifi Builder

Tools zum Erstellen des Lifi VirtualBox Images.

Mit diesem Repo kann die Lifi `.vdi`-Datei generiert werden.

Es wird auch automatisch ein Zip-Archiv für den Release erzeugt.

## Abhängigkeiten

- [VirtualBox 7](https://www.virtualbox.org/) (https://www.linuxcapable.com/how-to-install-virtualbox-7-0-on-ubuntu-22-04-lts/)
- GNU coreutils
- `make`
- `wget`
- `tar`
- `gzip`
- `genisoimage`
- `xorriso`
- `sshpass`
- `zip`
- `sha256sum`
- `python3`
- `pip3`
<!--
- `7z` (apt-get install p7zip-full p7zip-rar)
- `squashfs-tools`
- `sudo`
-->

Funktioniert nur auf amd64-bit Prozessoren.

Mindestens 25GB freien Festplattenspeicher.

## Release bauen

```bash
make clean-all
make release    # Alternative zum Entwickeln: LIFIBUILDINDEVMODE=true make release
make backup
```

Einen Release zu bauen dauert meist ca. eine Stunde.

Der Release kann danach im `releases` Ordner gefunden werden.

Das aktuelle Datum wird für die Versionierung von dem Release verwendet.

Es ist empfohlen die Ausgabe von dem `make release`-Befehl in einer entsprechenden `Lifi-YYYYMMDD.buildlog`-Datei neben dem Release zu speichern.
Dies sollte **vor** dem Backup geschehen.

## Auf M1 MacBook bauen

<!--0. (`brew install coreutils`) -->

1. UTM installieren
2. Ubuntu Server for Arm ISO runterladen: https://ubuntu.com/download/server/arm (22.04.1 LTS)
3. In UTM VM erstellen.
    1. Virtualisieren
    1. Linux
    1. ISO auswählen
    1. Speicherplatz: 30GB
    1. Name: Lifi
4. VM starten
5. Ubuntu installieren
    1. Sprache: English
    1. Keyboard Layout: German (Macintosh)
    1. Use an entire disk
        - [ ] Set up this disk as an LVM group
    1. Profile setup
        - Your name: Lifi
        - Server name: lifiarm
        - username: lifi
        - password: lifi
6. VM "neustarten" (bei blinkendem cursor beenden)
7. ISO "CD" entfernen
8. VM starten
9. Lifi installieren:
    - `sudo apt update`
    - `sudo apt install -y ubuntu-desktop-minimal git ansible cowsay`
    - `sudo git clone --recurse-submodules https://gitlab.gwdg.de/lifi/ansible-role-lifi.git /opt/lifi `
    - `/opt/lifi/files/update`
    - `echo 0 | sudo tee /var/opt/lifi_last_successful_update`
10. VM neustarten
11. VM herunterfahren
12. VM exportieren (sharen)
13. `tar -czvf Lifi-2022MMDD.tgz Lifi-2022MMDD.utm`
14. `shasum -a 256 Lifi-2022MMDD.tgz | tee Lifi-2022MMDD.sha256sum`


## Test

Getestet auf Ubuntu 22.04.1.

## Sonstige Links

- https://help.ubuntu.com/community/LiveCDCustomization
- https://wiki.ubuntu.com/UbiquityAutomation
- https://github.com/covertsh/ubuntu-preseed-iso-generator/blob/main/ubuntu-preseed-iso-generator.sh
- https://wiki.ubuntu.com/Ubiquity
- https://wiki.ubuntu.com/DesktopCDOptions
- https://wiki.debian.org/DebianInstaller/Preseed


## Klausur Version

Lifi kann auch im E-Prüfungsraum für Klausuren verwendet werden.

### Konfigurationsordner

Bevor ein Klausur Release gebaut werden kann, muss ein Konfigurationsordner erstellt werden.
Dieser sollte am Besten im [Prüfungen Repo](https://gitlab.gwdg.de/lifi/lifi-pruefungen) sein.

In diesem Repo hier sollte `current_exam_config` ein Symlink auf diesen Konfigurationsordner sein.
z.B.:
```bash
rm -f current_exam_config ; ln -sv ../lifi-pruefungen/ws202223/info1/ current_exam_config
```

Was der Konfigurationsordner beinhalten soll, steht in der [README vom Prüfungen Repo](https://gitlab.gwdg.de/lifi/lifi-pruefungen/-/blob/main/README.md).

### Klausur Release bauen

```bash
make clean-all
make release-exam    # Alternative zum Entwickeln: LIFIBUILDINDEVMODE=true make release-exam
make backup
```

Einen Klausur Release zu bauen dauert meist ca. eine Stunde.

Der Klausur Release kann danach im `releases` Ordner gefunden werden.

Das aktuelle Datum, das Wort "Exam" und die ID aus dem Konfigurationsordner der Klausur wird für die Versionierung (der Releasename) von dem Release verwendet.

Es ist empfohlen die Ausgabe von dem `make release-exam`-Befehl in einer entsprechenden `RELEASENAME.buildlog`-Datei neben dem Release zu speichern.
Dies sollte **vor** dem Backup geschehen.

**BEVOR EIN KLAUSUR RELEASE IN EINER KLAUSUR TATSÄCHLICH VERWENDET WERDEN KANN, IMMER DAS IMAGE SORGFÄLTIG TESTEN!!!**

### Klausur Release deployen

Um einen Release zum E-Prüfungsraum (bzw. auf den Server wo sich der E-Prüfungsraum das Image holt) zu deployen kann man einfach
```bash
make deploy-exam
```
ausführen.

Der Befehl wird die aktuellen Klausur Releases aus dem `releases` Ordner anzeigen und fragen welchen man daraus deployen möchte. Meistens ist die korrekte Auswahl `1`.

Nachdem man das ausgewählt und überprüft hat, muss man das nur mit `y` bestätigen und dann darauf warten, dass das Image fertig für den E-Prüfungsraum präpariert wird.

Nach einigen Minuten wird man nach dem Passwort für den SSH Zugang von dem Server gefragt. Danach wird das Image hochgeladen und ist damit deployed.

In `logs/deploy-exam-*.txt` wird der Log von dem Deployment gespeichert.

**BEVOR EIN KLAUSUR RELEASE DEPLOYED WIRD, IMMER DAS IMAGE SORGFÄLTIG TESTEN!!!**

## Backups

Ein Backup aller Lifi Releases befindet sich im Unix-Cluster der GWDG unter `/usr/users/a/doemming1/arbeit/lifi/releases`.

