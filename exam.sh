#!/bin/bash

set -e

pushd "$(dirname "$0")"

source ./common.sh

info "Changing Lifi to Exam Mode for ${EXAMID}"

if [ ! -L "./current_exam_config" ]; then
	die "./current_exam_config doesn't exist or isn't a Symlink."
fi


# Get Guest Additions that are compatible with E-Prüfungsraum (Version 6.1.12 r139181-4)
GUESTADDITIONSDLURL="https://download.virtualbox.org/virtualbox/6.1.12/VirtualBox-6.1.12-139181-Linux_amd64.run" # https://www.virtualbox.org/wiki/Download_Old_Builds_6_1
if [ ! -f "./build/VBoxGuestAdditions.iso" ]; then
	task "Getting Guest Additions iso file"
	rm -rf build/guestadditions
	mkdir -p build/guestadditions
	pushd build/guestadditions >/dev/null
	wget "$GUESTADDITIONSDLURL" -O VirtualBox.run
	chmod +x ./VirtualBox.run
	#./VirtualBox.run --list
	./VirtualBox.run --noexec --nox11  --nochown  --target .
	#ls -lah .
	bzip2 -d -c VirtualBox.tar.bz2 > VirtualBox.tar
	tar xf VirtualBox.tar
	mv -v ./additions/VBoxGuestAdditions.iso ..
	popd >/dev/null
	rm -rf build/guestadditions
fi

dumpvdidebuginfo

startvm


task "Set Hostname to 'Exam'"
# Note: This also disables lifiautoinstallguestadditions, as 'Exam' isn't a valid hostname in that script.
lifissh "sudo hostnamectl hostname Exam"

task "Uploading Exam Configuration directory"
lifissh "sudo rm -rvf /tmp/exam /opt/exam" || true
lifiscp "./current_exam_config" "/tmp/exam"
lifissh "sudo mv -v /tmp/exam /opt/exam"
lifissh "sudo chown -v -R lifi:lifi /opt/exam"
lifissh "sudo chmod -v -R o-rwx /opt/exam"


task "Uploading VBoxGuestAdditions.iso"
lifiscp "./build/VBoxGuestAdditions.iso" "/tmp/VBoxGuestAdditions.iso"

task "Installing guest additions"
GUESTADDINSTALLCOMMANDS="sudo mkdir -p /tmp/gaiso"
GUESTADDINSTALLCOMMANDS+=" && sudo mount /tmp/VBoxGuestAdditions.iso /tmp/gaiso -o loop,ro"
GUESTADDINSTALLCOMMANDS+=" && cd /tmp/gaiso"
GUESTADDINSTALLCOMMANDS+=" && (sudo ./VBoxLinuxAdditions.run || true)"
GUESTADDINSTALLCOMMANDS+=" && cd /"
GUESTADDINSTALLCOMMANDS+=" && sudo umount /tmp/gaiso"
GUESTADDINSTALLCOMMANDS+=" && sudo rm -rvf /tmp/VBoxGuestAdditions.iso /tmp/gaiso"
lifissh "/bin/bash -c \"${GUESTADDINSTALLCOMMANDS}\""

task "Run final commands to switch to Exam Mode"
# Note: lifissh won't work anymore after running this 'update' command, as it will change the password of the lifi user.
FINALCOMMANDS="update"
FINALCOMMANDS+=" && lifi-examctl full-reset"
FINALCOMMANDS+=" && lifi-examctl snapshot"
FINALCOMMANDS+=" && sudo shutdown -h +1"
lifissh "/bin/bash -c \"${FINALCOMMANDS}\""


waitforvmshutdown


popd
info "Done changing Lifi to Exam Mode for ${EXAMID}."
